#include "RMRandomMachine.hpp"

#include <random>
#include <cassert>

// create engine
std::default_random_engine engine;

int RMRandomMachine::GetInteter(int min, int max)
{
	// create generator with mersenne twister algorithm
	std::mt19937 generator(engine());

	// will throw exception if min bigger more than max
	assert(min < max);

	// create distributor
	std::uniform_int_distribution<int> distributor(min, max);

	return distributor(generator);
}
